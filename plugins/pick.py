
import random


from cloudbot import hook


@hook.command()
def pick(text, reply):
    """.pick item, item, item, ..."""

    args = text.split(",")

    try:
        return random.choice(args)
    except IndexError:
        return "No items to pick provided."

